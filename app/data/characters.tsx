export type CharacterData = {
  name: string;
  nickname?: string;
  slug?: string;
  faction: 'alliance' | 'horde' | 'unaffiliated';
  race?: string;
  class?: string;
  bio: string;
  listImage?: string;
  featuredImage?: string;
  backgroundImage?: string;
  gallery?: string[];
};

const characterData: CharacterData[] = [
  { name: "Velansalia", nickname: "Vela", faction: 'horde', race: "bloodelf", class: "mage", bio: "Character's Biography", backgroundImage: "", listImage: "images/featured/vela_list.jpg", featuredImage: "", gallery: ["/images/art/vela_1.jpg", "/images/art/vela_2.jpg", "/images/art/vela_3.jpg", "/images/art/vela_4.jpg", "/images/art/vela_5.jpg"] },
  { name: "Cerastrasza", nickname: "Cera", faction: 'horde', race: "bloodelf", class: "red_dragonflight", bio: "Character's Biography", backgroundImage: "", featuredImage: "", gallery: [] },
  { name: "Faerin", faction: 'horde', race: "bloodelf", bio: "Character's Biography", backgroundImage: "", featuredImage: "", gallery: [] },
  { name: "Fulidia", nickname: "Lidia", faction: 'horde', race: "bloodelf", class: "priest", bio: "Character's Biography", backgroundImage: "", featuredImage: "", gallery: [] },
  { name: "Lyndori", nickname: "Lynn", faction: 'horde', race: "bloodelf", class: "warlock", bio: "Character's Biography", backgroundImage: "", featuredImage: "", gallery: [] },
  { name: "Meradite", faction: 'unaffiliated', race: "lightforged", class: "bronze_dragonflight", bio: "Character's Biography", backgroundImage: "", featuredImage: "", gallery: [] },
  { name: "Nylathria", nickname: "Nyla", faction: 'horde', race: "voidelf", bio: "Character's Biography", backgroundImage: "", featuredImage: "", gallery: [] },
  { name: "Syenthe", faction: 'horde', race: "bloodelf", class: "hunter", bio: "Character's Biography", backgroundImage: "", featuredImage: "", gallery: [] },
  { name: "Syledrea", faction: 'horde', race: "bloodelf", class: "paladin", bio: "Character's Biography", backgroundImage: "", featuredImage: "", gallery: [] },
  { name: "Vylinda", nickname: "Vy", faction: 'horde', race: "bloodelf", class: "rogue", bio: "Character's Biography", backgroundImage: "", featuredImage: "", gallery: [] },
  { name: "Elevia", nickname: "Eve", faction: 'unaffiliated', race: "voidelf", bio: "Character's Biography", backgroundImage: "", featuredImage: "", gallery: [] },
];

export default characterData.map(({ slug = "", ...rest }) => (
  {
    slug: slug || rest.name.toLowerCase().replace(/ /g, "-"),
    ...rest
  }
));

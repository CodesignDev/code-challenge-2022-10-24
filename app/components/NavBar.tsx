import { Disclosure, Menu, Transition } from "@headlessui/react";
import { ArrowDownIcon, Bars3Icon, BellIcon, ChevronDownIcon, UserIcon, XMarkIcon } from "@heroicons/react/24/solid";
import classNames from "classnames";
import React, { FC } from "react";

type NavBarProps = {
  characters?: { name: string; nickname?: string }[];
};

export default function NavBar({ characters }: NavBarProps) {
  return (
    <div className="bg-white shadow">
      <div className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
        <div className="flex h-16 justify-between">
          <div className="flex">
            <div className="flex flex-shrink-0 space-x-2 items-center">
              <a href="/" className="flex space-x-2 items-center text-gray-900">
                <div>
                  <UserIcon className="h-8 w-8 text-gray-500" />
                </div>
                <div className="sm:text-xl">Code's Characters</div>
              </a>
            </div>
          </div>
          {characters && (
            <div className="hidden sm:ml-6 sm:flex sm:space-x-8">
              <Menu as="div" className="inline-flex relative">
                <div className="inline-flex items-center border-b-2 border-transparent px-1 pt-1 text-sm font-medium text-gray-500 hover:border-gray-300 hover:text-gray-700">
                  <Menu.Button className="flex">
                    Characters
                    <ChevronDownIcon className="w-5 h-5 ml-1 -mr-1" />
                  </Menu.Button>
                </div>
                <Transition
                    as={React.Fragment}
                    enter="transition ease-out duration-100"
                    enterFrom="transform opacity-0 scale-95"
                    enterTo="transform opacity-100 scale-100"
                    leave="transition ease-in duration-75"
                    leaveFrom="transform opacity-100 scale-100"
                    leaveTo="transform opacity-0 scale-95"
                >
                  <Menu.Items className="z-10 origin-top-right absolute top-16 right-0 mt-2 w-48 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5">
                    {characters.map((character, i) => (
                      <Menu.Item key={i}>
                        {({ active }) => (
                          <a
                            className={classNames({
                              'group flex w-full px-2 py-2 items-center space-x-1 rounded-md text-left text-sm leading-5 text-gray-700': true,
                              'bg-gray-100': active,
                            })}
                            href={`/characters/${character.name.toLocaleLowerCase()}`}>
                            <span>{character.name}</span>
                            {character.nickname && (
                              <span className="text-gray-400 text-xs">({character.nickname})</span>
                            )}
                          </a>
                        )}
                      </Menu.Item>
                    ))}
                  </Menu.Items>
                </Transition>
              </Menu>
            </div>
          )}
        </div>
      </div>
    </div>
  )
}

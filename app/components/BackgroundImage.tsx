import classNames from "classnames";

export type BackgroundImageProps = {
  src: string;
  alt: string;
  className?: string;
  align?: string;
}

export default function BackgroundImage({ src, alt = "", className = "", align }: BackgroundImageProps) {
  return (
    <div
      className={classNames({
        "opacity-50 absolute h-full bg-contain bg-no-repeat": true,
        [className]: className !== '',
        "left-0 bg-left": align === "left",
        "right-0 bg-right": align === "right",
      })}
      style={{backgroundImage: `url('${src}')`}}/>
  );
}

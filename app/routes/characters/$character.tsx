import { json, LoaderFunction } from "@remix-run/node";
import { useLoaderData, useParams } from "@remix-run/react";
import BackgroundImage from "~/components/BackgroundImage";
import NavBar from "~/components/NavBar";
import characterData from "~/data/characters";
import { getCharacterClass, getCharacterRace, isBaseClass } from "~/utilities";

export const loader: LoaderFunction = async ({ params }) => {
  const character = characterData.find(({ slug }) => slug.toLocaleLowerCase() === params.character?.toLocaleLowerCase());

  if (!character) {
    throw new Response("Not Found", { status: 404 });
  }

  return json({
    characters: characterData,
    character
  });
}

export function CatchBoundary() {
  return (
    <div className="">
      <NavBar />
      <main className="mx-auto mt-4 max-w-7xl px-4 sm:px-6 lg:px-8">
        <div className="flex flex-col justify-center flex-1">
          <div className="flex flex-col space-y-2 pb-10">
            <h2 className="text-2xl font-medium">Character not found!</h2>
            <span>
              <a href="/" className="inline-block border-b-2 border-gray-200 hover:border-gray-600">Return Home</a>
            </span>
          </div>
        </div>
      </main>
    </div>
  );
}

export default function CharacterView() {
  const { characters, character } = useLoaderData();
  return (
    <div className="relative h-full">
      <NavBar characters={characters} />
      <main className="mx-auto mt-4 max-w-7xl px-4 sm:px-6 lg:px-8 h-full">
        {character.backgroundImage && <BackgroundImage src={character.backgroundImage} alt="" className="opacity-5 absolute top-16 bottom-0 left-0 right-0 w-full h-full object-cover" />}
        <div className="flex flex-col justify-center space-y-2 sm:space-y-4 flex-1">
          <div className="flex flex-row justify-between flex-1">
            <div className="flex sm:flex-col items-baseline sm:items-start space-x-1 sm:space-y-2">
              <h1 className="text-xl sm:text-4xl font-medium sm:font-bold">{character.name}</h1>
              <div className="sm:hidden text-sm italic">also known as</div>
              <h2 className="sm:text-2xl font-medium">{character.nickname}</h2>
            </div>
            {isBaseClass(character.class) && (
              <img
                src={`/images/icons/${character.class}.png`}
                className="hidden sm:block w-24 h-24" />
            )}
          </div>
          <dl className="flex flex-row sm:flex-col space-x-1 sm:space-x-0">
            <div className="flex">
              <dt className="hidden sm:inline-block mr-1 font-medium">Race:</dt>
              <dd>{getCharacterRace(character.race)}</dd>
            </div>
            <div className="flex">
              <dt className="hidden sm:inline-block mr-1 font-medium">Class:</dt>
              <dd>{getCharacterClass(character.class)}</dd>
            </div>
          </dl>
          <div className="">{character.bio}</div>
          {character.gallery && character.gallery.length > 0 && (
            <div className="flex flex-col space-y-2">
              <h2 className="font-medium text-xl">Gallery</h2>
              <div className="container grid grid-cols-1 sm:grid-cols-3 gap-2 mx-auto">
                {character.gallery.map((image, index) => (
                  <div className="w-full rounded">
                    <img src={image} />
                  </div>
                ))}
              </div>
            </div>
          )}
        </div>
      </main>
    </div>
  );
}

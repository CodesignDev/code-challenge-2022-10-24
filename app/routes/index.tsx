import { json, LoaderFunction } from "@remix-run/node";
import { useLoaderData } from "@remix-run/react";
import NavBar from "~/components/NavBar";
import characterData from "~/data/characters";
import { races, classes } from "~/data";
import { getCharacterClass, getCharacterRace } from "~/utilities";
import BackgroundImage from "~/components/BackgroundImage";

export const loader: LoaderFunction = async() => {
  // return json(characterData.map(({name, nickname = ""}) => ({name, nickname})));
  return json(characterData);
};

export default function Index() {
  const characters = useLoaderData();
  return (
    <div className="h-full">
      <NavBar characters={characters} />
      <main className="mx-auto mt-4 max-w-7xl px-4 sm:px-6 lg:px-8 h-full">
        <div className="flex flex-col justify-start pb-4 flex-1 space-y-4 h-screen">
          {characters.map((character, i) => (
            <a
              key={i}
              className="group flex flex-col h-16 sm:h-32 p-2 sm:p-4 ring-2 justify-center sm:justify-start rounded-lg ring-gray-200 relative cursor-pointer hover:bg-gray-100"
              href={`/characters/${character.slug}`}>
              <div className="flex flex-row items-center space-x-1">
                <h1 className="text-xl sm:text-2xl">{character.name}</h1>
                {character.nickname && <small className="text-lg text-gray-600">({character.nickname})</small>}
              </div>
              <div className="flex flex-row items-center space-x-1">
                <span className="text-sm sm:text-base text-gray-600">{getCharacterRace(character.race)}</span>
                <span className="text-sm sm:text-base text-gray-600">{getCharacterClass(character.class)}</span>
              </div>
              {character.faction && <BackgroundImage src={`/images/icons/${character.faction}.png`} alt={character.faction} className="w-32 sm:w-80 top-0 bottom-0 right-0 sm:right-auto sm:left-0 bg-right sm:bg-left sm:opacity-10" />}
              {character.listImage && <BackgroundImage src={character.listImage} alt="" className="hidden sm:block w-32 sm:w-80 top-0 bottom-0 right-0" align="right" />}

              {/* {character.faction && <div className="opacity-50 absolute top-0 bottom-0 left-auto sm:left-0 w-32 sm:w-80 h-full bg-right bg-contain bg-no-repeat"} */}
              {/* {character.listImage && <div className="opacity-50 absolute top-0 bottom-0 right-0 w-32 sm:w-80 h-full bg-right bg-contain bg-no-repeat" style={{backgroundImage: `url('${character.listImage}')`}}></div>} */}
            </a>
          ))}
        </div>
      </main>
    </div>
  );
}

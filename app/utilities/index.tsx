import { classes, races } from "~/data"

export const getCharacterRace = (race: string) => {
  if (races.hasOwnProperty(race)) {
    return races[race];
  }

  return race;
}

export const getCharacterClass = (charClass: string) => {
  if (classes.hasOwnProperty(charClass)) {
    return classes[charClass];
  }

  return charClass;
}

export const isBaseClass = (charClass: string) => {
  const baseClasses = [
    'deathknight',
    'demonhunter',
    'druid',
    'evoker',
    'hunter',
    'mage',
    'monk',
    'paladin',
    'priest',
    'rogue',
    'shaman',
    'warlock',
  ];

  return baseClasses.includes(charClass?.toLocaleLowerCase());
}
